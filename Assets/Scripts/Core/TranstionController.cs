﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Manager.UI {
    public class TranstionController : MonoBehaviour {
        
        public static TranstionController Instance { get; private set; }
        
        private Canvas[] _canvas;
        private Image _transitionImage;


        private void Awake() {
            Singleton();
            Initialize();
        }

        private void Initialize() {
            //_transitionImage = GameObject.FindGameObjectWithTag("TranstionImage").GetComponent<Image>();
        }

        private void Singleton() {
            if (Instance == null) {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else {
                Destroy(gameObject);
            }
        }
        
        public void TranstionFade(float alphaValue, float duration, bool ignoreTimeScale = false) {
            if(_transitionImage != null)
                _transitionImage.CrossFadeAlpha(alphaValue, duration, ignoreTimeScale);
        }

 
        
    }
}