﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager.Audio {
    public class SampleController : MonoBehaviour{
        
        public Sample[] Samples;

        private void Awake() {
            AssignSampleData();
        }

        private void AssignSampleData() {
            foreach (var sample in Samples) {
                sample.Source = gameObject.AddComponent<AudioSource>();
                sample.Source.clip = sample.Clip;

                sample.Source.pitch = sample.Pitch;
                sample.Source.volume = sample.Volume;
                sample.Source.loop = sample.Loop;
            }
        }

        internal Sample FindSampleInArray(string sampleName) {
            var sample = Array.Find(Samples, sound => sound.Name == sampleName);
            return sample;
        }
    }
}