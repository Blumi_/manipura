﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager.Level;
using UnityEngine;

namespace Manager.Audio {
    public class AudioManager : MonoBehaviour {
        public static AudioManager Instance { get; set; }
        [SerializeField] private MusicController musicController;
        [SerializeField] private SampleController sampleController;


        private void Awake() {
            #region Singleton

            if (Instance == null) {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else {
                Destroy(gameObject);
            }

            #endregion
        }

        public string GetCurrentMusicTheme() => LevelManager.Instance._level.MusicTheme.name;
        
        public void PlayMusicOnStart(string musicName) {
            PlayMusic(musicName);
            FadeInMusic(.001f, .01f, musicName); 
        }
        
        public bool IsMusicSilent(string musicName) => musicController._IsMusicSilent(musicName);
        
        public void PlaySample(string sampleName) {
            var sample = sampleController.FindSampleInArray(sampleName);
            sample.Source.Play();
        }
        
        public void PlayMusic(string musicName) {
            var music = musicController.FindMusicInArray(musicName);
            music.Source.Play();
        }
        
        public void FadeInMusic(float fadeSpeed, float musicVolume, string musicName) {
            StartCoroutine(musicController._FadeInMusic(fadeSpeed, musicVolume, musicName));
        }
        
        public void FadeOutMusic(float fadeSpeed, string musicName) { 
            StartCoroutine(musicController._FadeOutMusic(fadeSpeed,  musicName));
        }

    }
}