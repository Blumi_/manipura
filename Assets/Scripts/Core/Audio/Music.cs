﻿using UnityEngine;
using UnityEngine.Audio;

namespace Manager.Audio {
    
    [System.Serializable]
    public class Music {
        public string Name;

        public AudioClip Clip;
        [Range(0f, 1f)] public float Volume;
        [Range(.1f, 3f)] public float Pitch;
        public bool Loop;

        [HideInInspector] public AudioSource Source;
    }
    
    [System.Serializable]
    public class Sample {
        public string Name;

        public AudioClip Clip;
        [Range(0f, 1f)] public float Volume;
        [Range(.1f, 3f)] public float Pitch;
        public bool Loop;

        [HideInInspector] public AudioSource Source;
    }
}