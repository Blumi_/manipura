﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager.Audio {
    public class MusicController : MonoBehaviour {
        
        public Music[] Musics;

        private void Awake() {
            AssignMusicData();
        }

        private void AssignMusicData() {
            foreach (var music in Musics) {
                music.Source = gameObject.AddComponent<AudioSource>();
                music.Source.clip = music.Clip;

                music.Source.pitch = music.Pitch;
                music.Source.volume = music.Volume;
                music.Source.loop = music.Loop;
            }
        }

        private int GetMusicID(string musicName) {
            return int.Parse(musicName.Substring(5));
        }

        internal Music FindMusicInArray(string musicName) {
            var music = Array.Find(Musics, sound => sound.Name == musicName);
            return music;
        }
        
        

        bool IsMusicMaxVolume( float musicVolume, string musicName) => Musics[GetMusicID(musicName)].Source.volume > musicVolume;

        internal bool _IsMusicSilent(string musicName) => Musics[GetMusicID(musicName)].Source.volume == 0;


        void FadeVolumeUp(float fadeSpeed, string musicName) => Musics[GetMusicID(musicName)].Source.volume += fadeSpeed;
        void MusicFadeVolumeDown(float fadeSpeed, string musicName) => Musics[GetMusicID(musicName)].Source.volume -= fadeSpeed;

        internal IEnumerator _FadeInMusic(float fadeSpeed, float musicVolume, string musicName) {
            yield return new WaitForSeconds(1f);
            while (!IsMusicMaxVolume(musicVolume, musicName)) {
                FadeVolumeUp(fadeSpeed, musicName);
                yield return new WaitForSeconds(.1f);
            }
        }

        internal IEnumerator _FadeOutMusic(float fadeSpeed, string musicName) {
            while (!_IsMusicSilent(musicName)) {
                MusicFadeVolumeDown(fadeSpeed, musicName);
                yield return new WaitForSeconds(.1f);
            }
        }
    }
}