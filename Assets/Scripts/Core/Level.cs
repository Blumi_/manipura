﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager.Level {
    public class Level : MonoBehaviour {
        [SerializeField] public Scene scene;
        [HideInInspector] public string NextLevelName => scene.name; 
        
        public int Chapter;

        public AudioClip MusicTheme;

        public Sprite LevelPreviewSprite;
    }

    
}