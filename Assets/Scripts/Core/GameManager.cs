﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Manager.Core {
    public class GameManager : MonoBehaviour {
        public static GameManager Instance { get; private set; }

        public string state;
        private void Awake() {
            Singleton();
        }
        
        private void Singleton() {
            if (Instance == null) {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else {
                Destroy(gameObject);
            }
        }
    }
} 