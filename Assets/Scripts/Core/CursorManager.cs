﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager.Audio;
using UnityEngine;
using Manager.Core;
using UnityEngine.UI;

namespace Manager.cursor{

    public class CursorManager : MonoBehaviour {

        [SerializeField] private Texture2D cursorTexture;

        private void Update() {
            if (GameManager.Instance.state.Equals("Game"))
                CursorEnabled(false);
            else
                CursorEnabled(true);
            
            if(Input.GetMouseButtonDown(0))
                AudioManager.Instance.PlaySample("UI_1");
        }
        

        private void CursorEnabled(bool enabled) => Cursor.visible = enabled;
        
        private void SetCurosrTexture(Texture2D cursorTexture) {
            Cursor.SetCursor(cursorTexture, new Vector2(0,0), CursorMode.Auto);
        }
        
        
    }
}

