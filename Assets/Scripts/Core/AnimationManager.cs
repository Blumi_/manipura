﻿using System;
using Player.Model;
using UnityEngine;

namespace Player.Animation {
    public class AnimationManager : MonoBehaviour{
        public Animator animator;

        private void Awake() => animator = GetComponent<Animator>();
        
        public void Play(string triggerName) => animator.SetTrigger(triggerName);
        public void SetAnimationSpeed(float speed) => animator.speed = speed;

        public bool IsAnimationFinished(string animationName) => animator.GetCurrentAnimatorStateInfo(0).IsName("MenuToGame");

        
    }
}