﻿using System;
using UnityEngine;
using Manager.Core;
using Manager.Audio;
using Manager.Level;
using Manager.UI;
using UnityEngine.Serialization;

namespace Manager.Model {
    public class ManagerModel : MonoBehaviour {
        public AudioManager audio;
        public LevelManager scene;
        public GameManager game;
        public TranstionController ui;

        private void Awake() {
            Initialize();
        }

        public void Initialize() {
            audio = FindObjectOfType<AudioManager>();
            scene = FindObjectOfType<LevelManager>();
            game = FindObjectOfType<GameManager>();
            ui = FindObjectOfType<TranstionController>();
        }
    }
}