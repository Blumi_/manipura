﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using Cinemachine;
using Manager.Model;
using Player.Model;


namespace Manager.Level {
    public class LevelManager : MonoBehaviour {
        public static LevelManager Instance { get; set; }

        private PlayerModel _player;
        private ManagerModel _manager;
        public Level _level;
        private CinemachineTargetGroup _cameraTarget;
        
 
        
        private void Awake() {
            SceneManager.sceneLoaded += OnSceneLoaded;
            Singleton();
            Initialize();
        }

        private void Singleton() {
            if (Instance == null) {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else {
                Destroy(gameObject);
            }
        }
        
        
        private void Initialize() {
            _player = FindObjectOfType<PlayerModel>();
            _manager = FindObjectOfType<ManagerModel>();
            _level = FindObjectOfType<Level>();
        }


        private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
           _manager.audio.PlayMusicOnStart(_manager.audio.GetCurrentMusicTheme());
           _manager.ui.TranstionFade(0f, 0.5f);
        }

        public void LoadScene(string sceneName) => SceneManager.LoadScene(sceneName);
        
        public void LoadNextLevel() => StartCoroutine(_LoadNextLevel());
        public void DeathScene() => StartCoroutine(_Death());

        

        private IEnumerator _LoadNextLevel() {
            _manager.game.state = "Transition";
            _manager.audio.FadeOutMusic(.005f, _level.MusicTheme.name);
            _player.manipulation.DisablePlayer();

            while (!_player.manipulation.IsPlayerScaledDown()) {
                _player.manipulation.MovePlayerToDestination(Vector3.zero);
                yield return new WaitForSeconds(.01f);
            }
            
            _manager.ui.TranstionFade(1f,.5f);
            yield return new WaitUntil(()=> _manager.audio.IsMusicSilent(_level.MusicTheme.name));
            yield return new WaitForSeconds(1);
            _manager.game.state = "Gameplay";
            SceneManager.LoadScene(_level.NextLevelName);
            _player.manipulation.EnablePlayer();
        }

        private IEnumerator _Death() {
            _manager.game.state = "Transition";
            _manager.audio.PlaySample("Death");
            _player.manipulation.DisablePlayer("Death");
            
            yield return new WaitForSeconds(.8f);
            
            _manager.ui.TranstionFade(1f,.1f);
            
            yield return new WaitForSeconds(.4f);
            
            _player.manipulation.EnablePlayer();
            _manager.ui.TranstionFade(0f,.2f);
            _manager.game.state = "Gameplay";
        }

    }
    
    
}