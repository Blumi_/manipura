﻿using System.Collections;
using System.Collections.Generic;
using Player.Animation;
using UnityEngine;

namespace Manager.Level{

    public class MenuManager : MonoBehaviour {
        private AnimationManager _animationManager;
        private LevelManager _levelManager;
  
        private void Awake() => Initalize();

        private void Initalize() {
            _animationManager = GetComponent<AnimationManager>();
            _levelManager = FindObjectOfType<LevelManager>();
        }
        
        public void LoadNewGame(string animationName) => StartCoroutine(_loadNewGame(animationName));

        private IEnumerator _loadNewGame(string animationName) {
            yield return new WaitUntil(()=> _animationManager.IsAnimationFinished(animationName));
            yield return new WaitForSeconds(2);
            _levelManager.LoadScene("level1");
        }
    }
}

