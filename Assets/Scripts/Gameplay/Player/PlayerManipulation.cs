﻿using System;
using UnityEngine;
using Player.Model;

namespace Player {
    public class PlayerManipulation : MonoBehaviour {
        private PlayerModel _player;
        
        private void Awake() => _player = gameObject.GetComponent<PlayerModel>();

        
        private void PlayerRespawn() {
            throw new NotImplementedException();
        }
        public void MovePlayerToDestination(Vector3 destination) {
            SetPlayerDestination(destination);
            ScaleDownPlayer();
            RotatePlayer();
        }
        private void SetPlayerDestination(Vector3 destination) {
            var playerTrans = _player.transform;
            playerTrans.position = Vector3.Lerp(playerTrans.position, destination, .1f);
        }
        private void ScaleDownPlayer() {
            var playerTrans = _player.transform;
            playerTrans.localScale = Vector3.Lerp(playerTrans.localScale, new Vector3() * 0, .05f);
        }
        private void RotatePlayer() {
            var playerTrans = _player.transform;
            playerTrans.eulerAngles += Vector3.forward * 4;
        }
        public void EnablePlayer() {
            _player.particle.SetMainParticleSystem();
            _player.physics.RigidbodyEnabled(true);
        }
        public void DisablePlayer(string action = "Movement") {
            _player.particle.SetMainParticleSystem(action);

            _player.controller.playerState = (action);
            _player.animation.Play(action);
            _player.physics.RigidbodyEnabled(false);
        }
        public bool IsPlayerScaledDown() => _player.transform.localScale == Vector3.zero;
    }
}