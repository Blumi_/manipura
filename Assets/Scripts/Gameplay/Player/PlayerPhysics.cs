﻿using System;
using UnityEngine;
using Player.Model;

namespace Player {
    public class PlayerPhysics : MonoBehaviour {
        private PlayerModel _player;
        

        [SerializeField] internal float speed;
        [SerializeField] internal float jumpForce;
        [SerializeField] private float airTime;

        private void Awake() => _player = GetComponent<PlayerModel>();

        
        public bool IsRaycastGrounded() {
            var bounds = _player.boxCollider2D.bounds;
            var raycastHit = Physics2D.BoxCast(bounds.center, bounds.size, 0f, Vector2.down, .2f, _player.controller.solidLayer);

            #region RayBox
            Color rayColor;
            if (raycastHit.collider != null) {
                rayColor = Color.green;
            } else {
                rayColor = Color.red;
            }
            Debug.DrawRay(bounds.center + new Vector3(bounds.extents.x, 0), Vector2.down * (bounds.extents.y + .2f), rayColor);
            Debug.DrawRay(bounds.center - new Vector3(bounds.extents.x, 0), Vector2.down * (bounds.extents.y + .2f), rayColor);

            #endregion

            Debug.DrawRay(bounds.center - new Vector3(bounds.extents.x, bounds.extents.y + .2f), Vector2.right * (bounds.extents.x * 2f), rayColor);
            return raycastHit.collider != null ? true : false;
        }
        public bool IsPlayerMoving() => IsMovingHorizontally() || IsMovingVertically();
        public bool IsMovingVertically() => _player.rigidbody2D.velocity.y > .8f || _player.rigidbody2D.velocity.y < -.8f;
        public bool IsMovingHorizontally() =>_player.rigidbody2D.velocity.x > .1f || _player.rigidbody2D.velocity.x < -.1f;
        internal bool CanJump() =>  IsRaycastGrounded() && _player.controller.playerState.Equals("Movement");

        internal void CheckAirTime() {
            if (IsRaycastGrounded()) airTime = 0f;
            else airTime += Time.deltaTime;
        }

        internal bool PlayerLanded() {
            return airTime > 0 && IsRaycastGrounded();
        }

        public bool RigidbodyEnabled(bool enable) => _player.rigidbody2D.simulated = enable;
        
        
    }
}