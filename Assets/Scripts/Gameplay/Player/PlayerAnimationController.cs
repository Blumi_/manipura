﻿using System;
using Player.Model;
using UnityEngine;

namespace Player.Animation {
    public class PlayerAnimationController : MonoBehaviour {
        
        private AnimationManager _animationManager;
        private PlayerModel player;

        private void Awake() => Initialize();

        private void Initialize() {
            player = GetComponent<PlayerModel>();
            _animationManager = GetComponent<AnimationManager>();
        }
        
        public void Play(string triggerName) => _animationManager.Play(triggerName);
        private void _SetAnimationSpeed(float speed) => _animationManager.SetAnimationSpeed(speed);


        public void DynamicAnimationSpeed() {
            if (player.physics.IsMovingHorizontally() && !player.physics.IsMovingVertically()) 
                player.animation._SetAnimationSpeed(3);
            else 
                player.animation._SetAnimationSpeed(1);
        }
        
    }
}