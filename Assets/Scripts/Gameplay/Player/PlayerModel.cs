﻿using Particle;
using UnityEngine;
using Player;
using Player.Animation;


namespace Player.Model {
    public class PlayerModel : MonoBehaviour {
        [SerializeField] public Rigidbody2D rigidbody2D;
        public PlayerAnimationController animation;
        public PlayerController controller;
        public BoxCollider2D boxCollider2D;
        public PlayerPhysics physics;
        public ParticleController particle;
        public PlayerManipulation manipulation;

        public ParticleSystem deathParticles, movementParticles;

        void Awake() {
            Initialize();
        }

        private void Initialize() {
            //rigidbody2D = FindObjectOfType<Rigidbody2D>();
            boxCollider2D = GetComponent<BoxCollider2D>();
            animation = FindObjectOfType<PlayerAnimationController>();
            controller = FindObjectOfType<PlayerController>();
            physics = FindObjectOfType<PlayerPhysics>();
            particle = FindObjectOfType<ParticleController>();
            manipulation = FindObjectOfType<PlayerManipulation>();

            deathParticles = GameObject.FindGameObjectWithTag("DeathParticles").GetComponent<ParticleSystem>();
            movementParticles = GameObject.FindGameObjectWithTag("MovementParticles").GetComponent<ParticleSystem>();
            
        }
        
    }
}