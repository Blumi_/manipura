﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player.Animation;
using Manager.Model;
using Player.Model;

namespace Player {
    public class PlayerController : MonoBehaviour {
        [SerializeField] private ManagerModel manager;
        [SerializeField] private PlayerModel player;

                    
        [SerializeField] internal LayerMask solidLayer;
        public string playerState;
        
        private void Awake() {
            manager = FindObjectOfType<ManagerModel>();
            player = gameObject.GetComponent<PlayerModel>(); 
        }

        private void Update() => PlayerJump();

        
        private void FixedUpdate() {
            PlayerMovement();
            PlayerLand();
            
            player.animation.DynamicAnimationSpeed();
            player.physics.CheckAirTime();    

            if (player.physics.IsPlayerMoving())
                player.particle.EnableParticles(player.movementParticles, true);
            else {
                player.particle.EnableParticles(player.movementParticles, false);
            }
        }

        private void PlayerJump() {
            if (Input.GetButtonDown("Jump") && player.physics.CanJump()) {
                manager.audio.PlaySample("Jump");
                player.animation.Play("Jump");
                player.rigidbody2D.velocity = Vector2.up * player.physics.jumpForce;
            }
        }

        
        
        private void PlayerLand() {
            if (player.physics.PlayerLanded()) {
                manager.audio.PlaySample("Land");
                player.animation.Play("Land");
            }
        }
        
        private void PlayerMovement() {
            var move = Input.GetAxis("Horizontal");
            player.rigidbody2D.velocity = new Vector2(move * player.physics.speed, player.rigidbody2D.velocity.y);
        }
        
        
        
    }
}
  