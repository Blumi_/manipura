﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Particle{

    public class TileController : MonoBehaviour {
        [SerializeField] private Tilemap _tilemap;
        private float maxAlpha = 1f;
        
        private Dictionary<Vector3Int, float> touchedTile = new Dictionary<Vector3Int, float>();

        private bool ContainsAlphaKey(Vector3Int tilePostion) => touchedTile.ContainsKey(tilePostion);
        private bool IsTileAlphaChangeble(Vector3Int tilePostion) => touchedTile[tilePostion] < 1; 
        
        
        private void ChangeTileOpacity(Vector3Int tilePosition, float alphaValue) {
            AddTileData(tilePosition);
            
            var currentAlpha = touchedTile[tilePosition] + alphaValue;

            if (currentAlpha <= 0f)
                touchedTile.Remove(tilePosition);
            else
                touchedTile[tilePosition] = Mathf.Clamp(currentAlpha, 0f, maxAlpha);
        }
        
        private void _setTileOpacity() {
            foreach (var tile in touchedTile) {
                float alphaPercent = tile.Value / maxAlpha;
                Color currentAlpha = new Color(1.0f, 1.0f, 1.0f, alphaPercent);
                
                _tilemap.SetTileFlags(tile.Key, TileFlags.None);
                _tilemap.SetColor(tile.Key, currentAlpha);
                _tilemap.SetTileFlags(tile.Key, TileFlags.LockColor);
            }
        }

        private void AddTileData(Vector3Int tilePosition) {
            if(!ContainsAlphaKey(tilePosition)) 
                touchedTile.Add(tilePosition, 0f);
        }
        
        public void SetTileOpacity(Vector2 worldPostion, float alpha) {
            Vector3Int tilePosition = _tilemap.WorldToCell(worldPostion);
            AddTileData(tilePosition);
            
            if (IsTileAlphaChangeble(tilePosition)) {
                ChangeTileOpacity(tilePosition, alpha);
                _setTileOpacity();  
            }
        }

    }
}

