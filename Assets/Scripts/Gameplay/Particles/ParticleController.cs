﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player.Model;
using UnityEngine;

namespace Particle {
    public class ParticleController : MonoBehaviour {
        [SerializeField] private PlayerModel player;
        [SerializeField] private TileController tileController;
        
        private ParticleSystem Particles => GetComponent<ParticleSystem>();
        private List<ParticleCollisionEvent> _collisionEvents;
        private void Awake() => player = FindObjectOfType<PlayerModel>();

        private void Start() {

            _collisionEvents = new List<ParticleCollisionEvent>();
        }

        public bool EnableParticles(ParticleSystem particles, bool enable) {
            var emission = particles.emission;
            return emission.enabled = enable;
        } 
        
        internal void SetMainParticleSystem(string mainParticles = "Movement") {
            switch (mainParticles) {
                case "Movement":
                    player.movementParticles.Play();
                    player.deathParticles.Play();
                    break;
                case "Death":
                    player.movementParticles.Stop();
                    player.deathParticles.Play();
                    break;
            }
        }
    
        private void OnParticleCollision(GameObject other)
        {
            int numCollisionEvents = Particles.GetCollisionEvents(other, _collisionEvents);

            int i = 0;
            
            while (i < numCollisionEvents)
            {
                Vector2 pos = _collisionEvents[i].intersection;

                tileController.SetTileOpacity((pos - new Vector2(.1f,.1f)), .01f);
                tileController.SetTileOpacity((pos + new Vector2(.1f,.1f)), .01f);
                i++;
            }
            
        }
        
    }
}